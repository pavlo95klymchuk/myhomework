import * as types from '../types';

export const setFormData = values => ({
    type: types.SET_FORM_DATA,
    payload: values
})

export const clearLocalStorage = () => ({
    type: types.CLEAR_LOCAL_STORAGE
})