import * as yup from 'yup';

const phoneValidate = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const Validate = yup.object().shape({
    firstName: yup
        .string()
        .min(3, 'Minimum first name length is 3 symbols')
        .max(20, 'Maximum first name length is 15 symbols')
        .required('This field is required'),
    lastName: yup
        .string()
        .min(3, 'Minimum last name length is 3 symbols')
        .max(20, 'Maximum last name length is 15 symbols')
        .required('This field is required'),
    age: yup
        .number()
        .min(18, 'Min age is 18 year')
        .max(120, 'It is unlikely that you are over 120 years old')
        .required('This field is required'),
    address: yup
        .string()
        .min(20, 'Minimum name length is 15 symbols')
        .max(100, 'Maximum name length is 50 symbols')
        .required('This field is required'),
    phoneNumber: yup
        .string()
        .min(10, 'Format number: 0884545454')
        .max(10, 'Format number: 0884545454')
        .matches(phoneValidate, 'Enter correct number')
        .required('This field is required'),
});

export default Validate