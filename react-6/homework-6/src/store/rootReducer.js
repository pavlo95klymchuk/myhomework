import { combineReducers } from 'redux'
import cardReducer from "./card/cardReducer"
import modalReducer from "./modal/modalReducer"
import formReduser from "./form/formReduser"
export default combineReducers({
    cardReducer,
    modalReducer,
    formReduser
})


