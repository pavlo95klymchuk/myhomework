import React, { useState, useEffect } from 'react'
import server from '../api/server'
import AllCard from '../components/AllCards/AllCard'
import { useHistory } from 'react-router-dom'

const Cart = (props) => {
    const clothes = server()
    const [cartClothes, setCartClothes] = useState([])
    const articleArr = localStorage.getItem('card');
    const parsedArr = JSON.parse(articleArr)
    // console.log(cartClothes);
    const history = useHistory();
    useEffect(() => {
        if (parsedArr) {
            setCartClothes(
                clothes.filter(obj => parsedArr.find(id => id === obj.article))

            )
            // history.go(0)
        }
    }, [parsedArr])

    // const testFun = () => {
    //     if (parsedArr) {
    //         return setCartClothes(
    //             clothes.filter(obj => parsedArr.find(id => id === obj.article))
    //         )
    //     }
    // }
    // testFun();
    return (
        <div>
            <h1>Cart :</h1>
            {cartClothes.length > 0 && <AllCard isCart clothesList={cartClothes} />}
        </div>
    )
}

export default Cart;