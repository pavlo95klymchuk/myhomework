import React from 'react';
import AllCard from '../AllCards/AllCard';

class CardList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            clothes: []
        }
    }

    componentDidMount() {
        fetch('cards.json')
            .then(response => response.json())
            .then(data => this.setState({ clothes: data }))
            ;
    }

    render() {
        return (
            <div>
                <AllCard clothesList={this.state.clothes} />
            </div>
        )
    }
}

export default CardList;