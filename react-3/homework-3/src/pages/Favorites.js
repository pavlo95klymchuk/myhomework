import React, { useState, useEffect } from 'react'
import server from '../api/server'
import AllCard from '../components/AllCards/AllCard'
const Favorites = () => {

    const [favsClothes, setFavsClothes] = useState([])
    const articleArr = localStorage.getItem('favorite');
    const parsedArr = JSON.parse(articleArr)
    const clothes = server()
    useEffect(() => {
        if (parsedArr) {
            setFavsClothes(
                clothes.filter(obj => parsedArr.find(id => id === obj.article))
            )

        } else {
            setFavsClothes(false)
        }
    }, [clothes, parsedArr])

    return (
        <div>
            <h1>Favorites :</h1>
            {favsClothes && <AllCard clothesList={favsClothes} />}
        </div>
    )
}

export default Favorites;