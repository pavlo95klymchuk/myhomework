import React, { useState, useEffect } from 'react'
import fetchCards from '../store/card/actions'
import AllCard from '../components/AllCards/AllCard'
import { useSelector, useDispatch } from 'react-redux'

const Favorites = () => {
    const dispatch = useDispatch()
    const phones = useSelector(store => store.cardReducer.phones)
    useEffect(() => {
        dispatch(fetchCards())
    }, [dispatch])

    const [cartPhones, setCartPhones] = useState([])
    const articleArr = localStorage.getItem('card');
    const parsedArr = JSON.parse(articleArr)

    useEffect(() => {
        if (parsedArr) {
            setCartPhones(
                phones.filter(obj => parsedArr.find(id => id === obj.article))
            )
        }
    }, [parsedArr])

    return (
        <div>
            <h1>Cart :</h1>
            {cartPhones.length > 0 && <AllCard isCart phonesList={cartPhones} />}
        </div>
    )
}

export default Favorites;