import * as types from '../types'

const initalState = {
    phones: []
};

const cardReducer = (state = initalState, action) => {
    switch (action.type) {
        case types.SET_CARDS:
            return {
                ...state,
                phones: action.payload
            }
        default:
            return state
    }
}


export default cardReducer