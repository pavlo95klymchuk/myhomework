import React from 'react'
import CardList from '../components/CardList/CardList'
import server from "../api/server"

const Home = () => {
    const clothes = server()
    return (
        <CardList clothesList={clothes} />
    )
}


export default Home;