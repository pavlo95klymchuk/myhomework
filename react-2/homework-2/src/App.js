import React from "react";
import CardList from "./components/CardList/CardList";

class App extends React.Component {
    render() {
        return (
            <>
                <CardList />
            </>
        );
    }
}

export default App;
