import React, { useEffect, useState } from 'react';
import AllCard from '../AllCards/AllCard';

const CardList = () => {
    const [clothes, setClothes] = useState([]);

    useEffect(() => {
        fetch('cards.json')
            .then(response => response.json())
            .then(data => setClothes(data))
            ;
    }, [])

    return (
        <div>
            <AllCard clothesList={clothes} />
        </div>
    )
}

export default CardList;