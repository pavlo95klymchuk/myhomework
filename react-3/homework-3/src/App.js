import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './pages/Home'
import Cart from './pages/Cart'
import Favorites from './pages/Favorites'
import Header from "./containers/Header"
const App = () => {

    return (
        <>
            <Header />
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/cart">
                    <Cart />
                </Route>
                <Route exact path="/favorites">
                    <Favorites />
                </Route>
            </Switch>
        </>
    );

}

export default App;
