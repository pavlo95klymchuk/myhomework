import Button from './Button';
import { act, render } from "@testing-library/react";


const id = 'button-id';


test('Render Button', () => {
    act(() => {
        render(<Button />)
    })
})

test('Add id for Button', () => {
    act(() => {
        render(<Button id={id} />)
    })
    const button = document.getElementById(id)
    expect(button.id).toBe(id)
})

test('Add someText on Button', () => {
    const someText = 'text';
    act(() => {
        render(<Button id={id} someText={someText} />)
    })
    const button = document.getElementById(id)
    expect(button.textContent).toBe(someText)
})

test('Add onClick for Button', () => {
    const onClick = jest.fn()
    act(() => {
        render(<Button id={id} onClick={onClick} />)
    })
    const button = document.getElementById(id)
    button.click()
    expect(onClick).toHaveBeenCalled()
})

test('Add backgroundColor for Button', () => {
    const backgroundColor = 'blue';
    act(() => {
        render(<Button id={id} backgroundColor={backgroundColor} />)
    })
    const button = document.getElementById(id)
    expect(button.style.backgroundColor).toBe(backgroundColor)
})