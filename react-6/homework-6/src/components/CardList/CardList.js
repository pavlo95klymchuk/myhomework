import React, { useEffect, useState } from 'react';
import AllCard from '../AllCards/AllCard';

const CardList = () => {
    const [phones, setPhones] = useState([]);

    useEffect(() => {
        fetch('cards.json')
            .then(response => response.json())
            .then(data => setPhones(data))
            ;
    }, [])

    return (
        <div>
            <AllCard phonesList={phones} />
        </div>
    )
}

export default CardList;