import "./App.css";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    state = {
        openModal: false,
        paramModal: null
    }
    openModal = (evt) => {
        if (evt.target.id === "firstBtn") {
            this.setState({ openModal: true, paramModal: "addFirst" })
        } else if (evt.target.id === "secondBtn") {
            this.setState({ openModal: true, paramModal: "addSecond" })
        }
    };
    closeModal = (evt) => {
        if (evt.target.classList.contains("modal_overlay") || evt.target.classList.contains("modal_close_btn") || evt.target.classList.contains("modal_buttons")) {
            this.setState({ openModal: false, paramModal: null })
        }
    };
    render() {
        const { openModal, paramModal } = this.state;
        return (
            <div className={"App"}>
                <div className={"btn_container"}>
                    <Button backgroundColor={"lightgreen"} onClick={this.openModal} someText={"First Btn"} id={"firstBtn"} />
                    <Button backgroundColor={"aqua"} onClick={this.openModal} someText={"Second Btn"} id={"secondBtn"} />
                </div>
                {openModal && (
                    <Modal closeModal={this.closeModal} header={paramModal === "addFirst" ? "Some heading 1" : "Some heading 2"} text={paramModal === "addSecond" ? "Some text to first modal." : "Some text to second modal."} closeButton={true}
                        actions={
                            <>
                                <button className={"modal_buttons"} onClick={this.closeModal}>Ok</button>
                                <button className={"modal_buttons"} onClick={this.closeModal}>Cancel</button>
                            </>
                        }
                    />
                )

                }
            </div>
        );
    }
}

export default App;
