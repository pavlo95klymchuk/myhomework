import Modal from "./Modal";
import { act, render } from "@testing-library/react";

test('Render Modal', () => {
    act(() => {
        render(<Modal />)
    })
})

test('Add header for Modal', () => {
    const header = 'Some text';
    act(() => {
        render(<Modal header={header} />)
    })
    const headerToText = document.getElementsByTagName('h2')[0].textContent;
    expect(headerToText).toBe(header)
})

test('Add text for Modal', () => {
    const text = 'Some text';
    act(() => {
        render(<Modal text={text} />)
    })
    const modalText = document.getElementsByClassName('modal_text')[0].textContent;
    expect(modalText).toBe(text)
})

test('Add onClick for Modal', () => {
    const onClick = jest.fn()
    act(() => {
        render(<Modal onClick={onClick} />)
    })
    const modalOverlay = document.getElementsByClassName('modal_overlay')[0]
    modalOverlay.click()
    expect(onClick).toHaveBeenCalled()
})

test('Add closeButton for Modal', () => {
    expect(document.getElementsByClassName('modal_close_btn')[0]).toBeFalsy()
    act(() => {
        render(<Modal closeButton />)
    })
    const modalCloseBtn = document.getElementsByClassName('modal_close_btn')[0]
    expect(modalCloseBtn).toBeDefined()
})

test('Add actions for Modal', () => {
    const actions = <button>Some button</button>
    expect(document.getElementsByTagName('button')[0]).toBeFalsy()
    act(() => {
        render(<Modal actions={actions} />)
    })
    const button = document.getElementsByTagName('button')[0]
    expect(button).toBeDefined()
    expect(button.textContent).toMatch(/some button/i)
})